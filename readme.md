# About vimconfig

Vimconfig is my personal collection of vim plugins, as well as my own tiny changes.

# Installing

Simply clone it and make a symlink from the included vimrc file:

    git clone https://bitbucket.org/kayph/vimconfig ~/.vim && ln -sf ~/.vim/vimrc ~/.vimrc

