execute pathogen#infect()
syntax on

filetype on
filetype plugin on
filetype plugin indent on

set omnifunc=syntaxcomplete#Complete
set completeopt=longest,menuone
" on enter select highlighted menu item
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR> '
inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
  \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

set nocompatible
set novisualbell

set laststatus=2
set showmode
set ruler
set number
set incsearch
set hlsearch
set wrapscan
set ignorecase
set smartcase
set magic

set tabstop=4		" width of tab character
set shiftwidth=4	" indentation step
set softtabstop=4	" tab key step
set expandtab		" expand tabs to spaces
set smarttab		" smarter tab

autocmd FileType html,xhtml,css,xml,xslt set shiftwidth=2 softtabstop=2
autocmd FileType css,scss set shiftwidth=2 softtabstop=2
autocmd FileType vim,lua,nginx set shiftwidth=2 softtabstop=2
autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0

let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
let g:indent_guides_enable_on_vim_startup = 1

" set g:jellybeans_use_lowcolor_black = 0
colorscheme jellybeans

" Ultisnips
let g:UltiSnipsExpandTrigger="<tab>"

" Emmet (html/css)
let g:user_emmet_install_global = 0
autocmd FileType html,css,scss EmmetInstall
let g:user_emmet_leader_key = '<C-Z>'

